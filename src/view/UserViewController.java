package view;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.io.File;
import java.io.FileNotFoundException;

import model.Admin;
import model.Album;
import model.Photo;
import model.User;
import application.Photos;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class UserViewController {
	
	@FXML
    private ListView<String> AlbumNames;
	
	private BorderPane rootLayout;
	@FXML
	private Button createAlbum;
	
	@FXML
	private TextField TextAlbumName;
	
	@FXML
	private Button deleteAlbum;
	
	@FXML
	private Button SearchAlbumViaTag;
	
	@FXML
	private Button SearchAlbumDate;
	
	@FXML
	private TextField StartDate;
	
	@FXML
	private TextField EndDate;
	@FXML
	private TextField TagType;
	
	@FXML
	private TextField TagValue;
	
	@FXML
	private Button FinalizeSearch;
	
	@FXML
	private ObservableList<Album> AlbList = FXCollections.observableArrayList();
	
	 
	 
	 
	 @FXML
	 TableView<Album> albumTable;

	//private Main Main;
	
		@FXML 
		TableColumn<Album, String> albumNameColumn;
	 
	 @FXML
	  TableColumn<Album, Integer> numberPhotosColumn;
	 
	 @FXML
	 TableColumn<Album, Calendar> earliestDate;
	 
	
	  @FXML
	  TableColumn<Album, Calendar> latestDate;
	
	
	private User user; 
	
	

	public void setMain(User u) {
		user = u; 
		
		for (Album a : user.getAlbumList()) {
			if(!AlbList.contains(a))AlbList.add(a);
		}
        albumTable.setItems(AlbList);// Perfectly Ok here, as FXMLLoader already populated all @FXML annotated members. 
    
		
		System.out.println(AlbList.size());
		
		if(user.getAlbumNames().size()==0) {
			return;
		}
		else {
		if(!AlbList.isEmpty())
		//albumTable.setItems(AlbList);
		albumTable.getSelectionModel().selectFirst();
		}
	}
	@FXML
	public void addAction() throws IOException {
		
		String alb = TextAlbumName.getText();
		
	user.addAlbum(new Album(alb));
	AlbList.add(user.getAlbum(alb));
		System.out.println(alb);
		System.out.println(user.getAlbumNames());
		if(user.getAlbumNames().size()==0) {
			return;
		}
		else {
	
			albumTable.setItems(AlbList);
		
		Admin.write();
			
		}
		
	}
	

    @FXML
    public void initialize() {
    	
		albumNameColumn.setCellValueFactory(new PropertyValueFactory<>("albumName"));
		numberPhotosColumn.setCellValueFactory(new PropertyValueFactory<>("PhotoCount"));
		earliestDate.setCellValueFactory(new PropertyValueFactory<>("EarliestDate"));
		latestDate.setCellValueFactory(new PropertyValueFactory<>("LatestDate"));
		albumTable.getColumns().clear();
		albumTable.getColumns().addAll(albumNameColumn, numberPhotosColumn, earliestDate, latestDate);
		albumTable.refresh();
    }
    
	@FXML
	public void delAction() throws IOException{
		int selectedIndex = albumTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
        	user.deleteAlbum(user.getAlbumList().get(selectedIndex));
        	AlbList.remove(selectedIndex);
        //	user.getAlbumList().remove(selectedIndex);
        //	user.getAlbumNames().remove(selectedIndex);
     
        } else {
            // Nothing selected.
        	Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(Photos.getPrimaryStage());
            alert.setTitle("Error");
            alert.setHeaderText("No User Selected");
            alert.setContentText("Please select a user from the table.");
            alert.showAndWait();
        }
        
	albumTable.setItems(AlbList);
	albumTable.refresh();
		if (!AlbList.isEmpty()) {
			albumTable.getSelectionModel().selectFirst();
		}
		Admin.write();
		
	}
	public void handleLogout() throws IOException {

		
		Photos.showLoginView();
	}
	
	@FXML
	public void renameAlbum() throws FileNotFoundException, IOException {
	
		int index =	albumTable.getSelectionModel().getSelectedIndex();
		String g = TextAlbumName.getText();
	
	System.out.println(AlbList);
	System.out.println(g);
	System.out.println(index);
		AlbList.get(index).setAlbumName(g);
		
	user.getAlbumList().get(index).setAlbumName(g);
	Album temp = AlbList.get(index);
	AlbList.remove(index);
	AlbList.add(index,temp);
	albumTable.refresh();
	albumTable.setItems(FXCollections.observableArrayList(AlbList));

	Admin.write();
	}
	
	public void handleAlbum() {
	}
	public void AlbumView(Album a) {
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Photos.class.getResource("/view/Album_View.fxml"));
		AnchorPane Alb;
		try {
			Alb = (AnchorPane) loader.load();
			 Photos.getRootLayout().setCenter(Alb);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		AlbumViewController controller = loader.getController();
		
		controller.setMain(a,user);
	
	
    }
	
	public void openAlbum() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Photos.class.getResource("/view/Album_View.fxml"));
		AnchorPane Alb;
		try {
			Alb = (AnchorPane) loader.load();
			 Photos.getRootLayout().setCenter(Alb);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Album a = albumTable.getSelectionModel().getSelectedItem();
	 
		AlbumViewController controller = loader.getController();
		controller.setMain(a, user);
	}


}
