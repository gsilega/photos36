package view;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import java.io.File;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;
import model.Admin;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
import application.Photos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import javafx.stage.FileChooser;


public class AlbumViewController {
//	@FXML
//	private ObservableList<Photo> photoList = FXCollections.observableArrayList();
	
	 @FXML  TableView<Photo> PhotoTable = new TableView<Photo>();
	 @FXML Button DisplayPhoto;
		@FXML
		private  ObservableList<Photo> PhotoList = FXCollections.observableArrayList();

	
		@FXML
		 TableColumn<Photo, ImageView> ImageFile;
		
	 
	 @FXML

	  TableColumn<Photo, String> PhotoCaption;
	 
	 @FXML
		private Button ButtonAddPhoto;
	 
	 @FXML
		private Button searchButton;
	 @FXML
		private Button ButtonRemovePhoto;
	 
	 @FXML
	 private Button ButtonCopyPhoto;
	 
	 @FXML
	 private Button ButtonMovePhoto;
	 
	 @FXML TextField TextFieldEditCaption;
	 
	 @FXML TextField TextFieldAddTag;
	 
		
		@FXML
		private TextField TagType;
		
		@FXML
		private TextField TagValue;
		
		@FXML
		private TextField caption;
		
		@FXML
		private Button ButtonEditCaption;
		
		@FXML
		private Button AddTagToSearch;
		
		@FXML
		private Button ButtonAddTag;
		
		@FXML
		private Button ButtonRemoveTag;
		
		@FXML
		private Button FinalizeSearch;
		
		@FXML
		private Button ButtonDisplayPhoto;
		
		@FXML
		private Button ButtonViewSlideshow;

		@FXML
		private Button ButtonBack;

		@FXML
		private Button logout;
		
		@FXML
		private static Album album;
		
		@FXML Scene Move;
		private User user;
		
	
	 public void setAlbum(Album a) { album =a;}
	 public void setUser(User u) { user =u;}
	
		public void setMain(Album a, User u) { int q =0;
			System.out.println(q++);
			album = a; 
			user = u;
			System.out.println(q++);
			for (Photo p : album.getPhotos()) {
				System.out.println(p.getPictureFile());
				if(!PhotoList.contains(p))PhotoList.add(p);
				System.out.println(PhotoList.get(PhotoList.indexOf(p)).getPictureFile());
			}
			System.out.println( PhotoList.size() + " size of photolist"
					);
			
			PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));
			

			
			if(album.getPhotoCount()==0) {
				return;
			}
			else {
			if(!PhotoList.isEmpty())
			//albumTable.setItems(AlbList);
			PhotoTable.getSelectionModel().selectFirst();
			}
		}
		
		@FXML
		public void addPicture() throws FileNotFoundException, IOException {
			
			 FileChooser fileChooser = new FileChooser();
			 fileChooser.setTitle("Save Image");
            
             FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
             FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
             fileChooser.getExtensionFilters().add(extFilterPNG);
             fileChooser.getExtensionFilters().add(extFilterJPG);
             File file = fileChooser.showOpenDialog(null);
             
             if(file ==null)return;
             
            Image m = new Image(file.toURI().toString());
           
		String cap = TextFieldEditCaption.getText();
     
             Photo p = new Photo(cap,file);
             album.addPhoto(p);
             PhotoList.add(p);
    //     user.update(album);
             Admin.write();
             
             System.out.println(PhotoList.size());
             System.out.println(PhotoList);
             
           
			PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));
			//Admin.write();
		}
		

@FXML void editCaption() throws FileNotFoundException, IOException{
	int index =	PhotoTable.getSelectionModel().getSelectedIndex();
	String g = TextFieldEditCaption.getText();
	System.out.println("1s5 " + g);
	PhotoList.get(index).setCaption(g);
	System.out.println("2nd " + PhotoList.get(index).getCaption());
album.getPhoto(album.getPhotos().get(index)).setCaption(g);
System.out.println("3rd " + album.getPhoto(album.getPhotos().get(index)).getCaption());
Photo temp = PhotoList.get(index);
PhotoList.remove(index);
PhotoList.add(index,temp);
repopulate();
PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));

Admin.write();
}
		
		
		@FXML 
		public void DelPicture() throws FileNotFoundException, IOException {
		int index =	PhotoTable.getSelectionModel().getSelectedIndex();
		PhotoList.remove(index);
	album.deletePhoto(album.getPhotos().get(index));
	PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));
//	user.update(album);
	Admin.write();
		}
		
		@FXML
		public void initialize() {
			
			 
			ImageFile.setCellValueFactory(new PropertyValueFactory<>("ImageView"));
			PhotoCaption.setCellValueFactory(new PropertyValueFactory<>("caption"));
			
			PhotoTable.getColumns().clear();
			PhotoTable.getColumns().addAll(ImageFile,PhotoCaption);
			PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));
			System.out.println( PhotoList.size() + " size of photolist"
					);
			
			PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));
			
		}
		
		
		
		
		
		public void repopulate(){
			PhotoTable.refresh();
		}
		
		@FXML void AddTag() throws FileNotFoundException, IOException { // on Add Tag 
			int index =	PhotoTable.getSelectionModel().getSelectedIndex();
		String t =  TagType.getText();
		String v = TagValue.getText();
		Tag n = new Tag(t,v);
		album.getPhotos().get(index).addTag(n);
		PhotoList.get(index).addTag(n);
		PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));

	PhotoList.get(index).TagtoString();
		System.out.println( "SIZE "  +PhotoList.get(index).getTags().size());
		TagType.clear();
		TagValue.clear();
		Admin.write();
		}
		
		@FXML void DeleteTag() throws FileNotFoundException, IOException { // on delete Tag
		int index =	PhotoTable.getSelectionModel().getSelectedIndex();
			String t =  TagType.getText();
			String v = TagValue.getText();
			Tag n = new Tag(t,v);
			album.getPhotos().get(index).removeTag(n);
			PhotoList.get(index).removeTag(n);
			PhotoTable.setItems(FXCollections.observableArrayList(PhotoList));
			Admin.write();
			TagType.clear();
			TagValue.clear();
		}
		

		@FXML void CopyPhoto() { 
			int selectedIndex =	PhotoTable.getSelectionModel().getSelectedIndex();
			   if (selectedIndex >= 0) {
				   Photo copy = PhotoList.get(selectedIndex);
				   PhotoTable.setItems(PhotoList);
				   PhotoTable.refresh();
				   User u = user;
				   Album a = album;
				  MoveView(copy, u, a);
				   
		        } else {
		            // Nothing selected.
		        	Alert alert = new Alert(AlertType.WARNING);
		            alert.initOwner(Photos.getPrimaryStage());
		            alert.setTitle("Error");
		            alert.setHeaderText("No Photo Selected");
		            alert.setContentText("Please select a photo from the table.");
		            alert.showAndWait();
		        }}
		
		
		
		@FXML void MovePhoto() {
			int selectedIndex =	PhotoTable.getSelectionModel().getSelectedIndex();
			   if (selectedIndex >= 0) {
				   Photo copy = PhotoList.get(selectedIndex);
				   PhotoList.remove(selectedIndex);
				   album.deletePhoto(copy);
				   PhotoTable.setItems(PhotoList);
				  
				   PhotoTable.refresh();
				   User u = user;
				   Album a = album;
				  MoveView(copy, u, a);
				   
		        } else {
		            // Nothing selected.
		        	Alert alert = new Alert(AlertType.WARNING);
		            alert.initOwner(Photos.getPrimaryStage());
		            alert.setTitle("Error");
		            alert.setHeaderText("No Photo Selected");
		            alert.setContentText("Please select a photo from the table.");
		            alert.showAndWait();
		        }
			   
		}
		public void MoveView(Photo p, User u, Album a) {
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Photos.class.getResource("/view/Copy_Move_Where.fxml"));
			AnchorPane Alb;
			try {
				Alb = (AnchorPane) loader.load();
				 Photos.getRootLayout().setCenter(Alb);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CopyMoveController word = loader.getController();
			
			word.setMain(p, u, a);
		
	    }
		
	@FXML	public void Search() {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Photos.class.getResource("/view/SearchOperations.fxml"));
			AnchorPane Alb;
			try {
				Alb = (AnchorPane) loader.load();
				 Photos.getRootLayout().setCenter(Alb);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SearchController word = loader.getController();
			
			word.setMain(album,user);
		}
		
		
		@FXML void Display() {
		
			int selectedIndex =	PhotoTable.getSelectionModel().getSelectedIndex();
			   if (selectedIndex >= 0) {
				   Photo copy = PhotoList.get(selectedIndex);
				   PhotoTable.setItems(PhotoList);
				   
		}
			   FXMLLoader loader = new FXMLLoader();
				loader.setLocation(Photos.class.getResource("/view/Display_Photo.fxml"));
				SplitPane Alb;
				try {
					Alb = (SplitPane) loader.load();
					 Photos.getRootLayout().setCenter(Alb);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				DisplayPhotoController word = loader.getController();
				
				word.setMain(PhotoList.get(selectedIndex));
		}
		
		
		public void handleLogout() throws FileNotFoundException, IOException { // need to add log out button
			Admin.write();
				Photos.showLoginView();
		}
		
		
		
		@FXML public void GoBack() throws FileNotFoundException, IOException {
			Admin.write();
			Photos.UserView();
		}

}