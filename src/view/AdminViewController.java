package view;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import model.Admin;
import model.User;
import application.Photos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AdminViewController {
	
	@FXML
    private ListView<String> userListView;
	
	
	@FXML
	private Button createUser;
	
	@FXML
	private Button deleteUser;
	
	@FXML
	private TextField TextFieldcreateUser;
	@FXML
	private ObservableList<User> obsList;

	private Photos Photos;


	public void setMain() throws ClassNotFoundException, IOException {
		this.Photos = Photos;
		BufferedReader br = new BufferedReader(new FileReader("serialized.dat"));     
		if (!(br.readLine() == null)) {Admin.read(); }
		
		ObservableList<String> obsList = FXCollections.observableArrayList(Admin.getUserListNames());
		userListView.setItems(obsList);
		
		if (!obsList.isEmpty()) {
			userListView.getSelectionModel().selectFirst();
		}
	}
	@FXML
	public void handleLogout() {
	
		
			Photos.showLoginView();
	
	}
	@FXML
	public void handleCreatUsere() throws IOException {
		String username = TextFieldcreateUser.getText();
	Admin.addUser(new User(username));
		ObservableList<String> obsList = 
	FXCollections.observableArrayList(Admin.getUserListNames());
		userListView.setItems(obsList);
		if (!obsList.isEmpty()) {
			userListView.getSelectionModel().selectFirst();
		}
		Admin.write();
		
	}
	@FXML
	public void handleDelete() throws FileNotFoundException, IOException {
		int selectedIndex = userListView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
        	Admin.getUserList().remove(selectedIndex);
        	Admin.getUserListNames().remove(selectedIndex);
        //	System.out.print(Admin.getUserListNames());
        //	System.out.print(Admin.getUserList());
        } else {
            // Nothing selected.
        	Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(Photos.getPrimaryStage());
            alert.setTitle("Error");
            alert.setHeaderText("No User Selected");
            alert.setContentText("Please select a user from the table.");
            alert.showAndWait();
        }
        ObservableList<String> obsList = FXCollections.observableArrayList(Admin.getUserListNames());
		userListView.setItems(obsList);
		if (!obsList.isEmpty()) {
			userListView.getSelectionModel().selectFirst();
		}
		Admin.write();
	}
	
	

}
