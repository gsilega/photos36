package view;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import java.io.File;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;
import model.Admin;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
import application.Photos;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import javafx.stage.Modality;

public class DisplayPhotoController {
	
	ObservableList<Tag> obsList = FXCollections.observableArrayList();
	
	ListView<Tag> tagList;
	
	 @FXML
	 TableView<Tag> TagTable;

	
		@FXML 
		TableColumn<Tag, String> TagTypeColumn;
	 
	 @FXML
	  TableColumn<Tag, String> TagValueColumn;
	 
	@FXML
	private ImageView imgFrame;
	
	@FXML
	private Label LabelCaption;
	
	@FXML
	private Label LabelDate;
	
	@FXML
	private Label LabelTags;
	
	@FXML
	private ListView<Tag> ListTags;
	
	@FXML
	private Button ButtonBack;
	


	Photo photo;
	// ------------------------------------------------------------------------------------
	
	@FXML
	public void GoBack() throws FileNotFoundException, IOException {
		// Main.AlbumView();
	}
	
	public void setMain(Photo p) {
		photo =p;
		Image img = new Image(photo.getPictureFile().toURI().toString());
		imgFrame.setImage(img);
		//imgFrame.setFitWidth(400);
		imgFrame.preserveRatioProperty();
		
		obsList.addAll(p.getTags());
		System.out.println(LabelCaption);
		LabelCaption.setText(p.getCaption());
		LabelDate.setText(p.getDate().toString());
		TagTable.setItems(obsList);
	}
	 @FXML
	    public void initialize() {
	    	
			TagTypeColumn.setCellValueFactory(new PropertyValueFactory<>("tagType"));
			TagValueColumn.setCellValueFactory(new PropertyValueFactory<>("tagValue"));
			
			TagTable.getColumns().clear();
			TagTable.getColumns().addAll(TagTypeColumn, TagValueColumn);
			TagTable.refresh();
	    }
	

}