package view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import application.Photos;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import model.Admin;
import model.Album;
import model.Photo;
import model.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import view.*;

public class CopyMoveController {
	
 @FXML Button AddToAlbum;
	 
	 @FXML ListView<String> AlbumListView = new ListView<String>();
	 @FXML
		private  ObservableList<String> AlbumList = FXCollections.observableArrayList();
	 Photo photo;
	 ArrayList<Album> albums;
	 Album currentAlbum;
	 User user;
	 
	 public void setMain(Photo p,User u, Album curr) {
		 photo = p;
		 albums = u.getAlbumList();
		 currentAlbum = curr;
		 user = u;
		 
		 System.out.println(u.getAlbumList());
		 for (int i =0; i<albums.size(); i++) {
			
				if(!AlbumList.contains(albums.get(i).getAlbumName())) {
					AlbumList.add(albums.get(i).getAlbumName());
				System.out.println(AlbumList.get(i));}
			}
			AlbumListView.setItems(AlbumList);
			
			AlbumListView.getSelectionModel().selectFirst();
	 }
	
	 
	 @FXML public void move() throws FileNotFoundException, IOException { // on add action
		 
		 Album go = albums.get(AlbumListView.getSelectionModel().getSelectedIndex());
		 
		 go.addPhoto(photo);
		 
		 GoBack();
		 
	 }
	 
	 @FXML public void GoBack() throws FileNotFoundException, IOException {
			Admin.write();
			UserViewController  m = new UserViewController();
		m.AlbumView(currentAlbum);
		}

	

	
	 
	 
}
