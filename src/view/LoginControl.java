package view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import application.Photos;
import model.Admin;
import model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginControl {
	
    @FXML 
    private Button loginButton;
    @FXML 
    private TextField usernameTextField;
    @FXML 
    private Text invalidUsernameText;
    
  
	@FXML
	protected void loginAttempt(ActionEvent e) throws ClassNotFoundException, IOException {

		String username = usernameTextField.getText();
		//System.out.println("lets see if " + username + " is in the list " + Admin.containsUser(username));
		if (username.equals("admin")) {
			// Call Admin_Subsystem if user is admin
			Photos.showAdminView();
			
		} else if (Admin.containsUser(username)) {
			
	User u = Admin.returnUser(username);
		Photos.SetUser(u);
			Photos.UserView();
		} else {
			// Display an invalid user message on the login screen if the username is invalid
			invalidUsernameText.setText("Invalid username.");
			Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(Photos.getPrimaryStage());
            alert.setTitle("Error");
            alert.setHeaderText("Invalid input");
            alert.setContentText("Bad input, not a valid user.");
            alert.showAndWait();
		}
		
	}
	
	@FXML public void initialize() throws ClassNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader("serialized.dat"));     
		if (br.readLine() == null) {return;}
		else 
		Admin.read();
	}

}