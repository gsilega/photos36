package view;

import java.io.IOException;

import model.Admin;
import application.Photos;
import javafx.fxml.FXML;

public class RootLayoutController {

private Photos Photos;
	
	/**
	 * This is called by the main application to return a reference
	 * to itself.
	 *
	 * @param Photos
	 */
	public void setMain(Photos Photos) {
        Photos = Photos;
    }
	
	/**
	 * This is called when the user logs out and returns user
	 * to the login view.
	 */
	@FXML
	private void handleLogout() throws IOException {
	
			Photos.showLoginView();
		
	}
	
	/**
	 * This is called when the user exits out of the application.
	 */


}
