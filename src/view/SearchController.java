
package view;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import java.io.File;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;
import model.Admin;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
import application.Photos;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.FileChooser;

public class SearchController {
	
	@FXML  TableView<Photo> PhotoTable = new TableView<Photo>();
	 
	


	@FXML
	 TableColumn<Photo, ImageView> ImageFile;
	
 
 @FXML TableColumn<Photo, String> PhotoCaption;
@FXML Button SearchTag;
@FXML Button SearchDate;
@FXML Button FinalizeSearchTag;
@FXML Button FinalizeSearchDate;
@FXML Button AddToTag;
@FXML Button cancelButton;
@FXML Button GoBackButton;
@FXML TextField TagType;
@FXML Button createAlbum;
@FXML TextField TagValue;
@FXML TextField newAlbname;
@FXML DatePicker first;

@FXML DatePicker second;
private ArrayList<Photo> SearchedReturn = new ArrayList<Photo>();
ObservableList<Photo> list = FXCollections.observableArrayList();
private ArrayList<Tag> TagsToSearch = new ArrayList<Tag>();
	
private Album album;
User user;
	public void setMain(Album a, User u) {
		album =a;
		user =u;
	}
	
	@FXML void DateVisibility(){
		
		TagType.setVisible(false);
		TagValue.setVisible(false);
		
		FinalizeSearchTag.setVisible(false);
		FinalizeSearchDate.setVisible(true);
		AddToTag.setVisible(false);
		SearchDate.setVisible(true);
		SearchTag.setVisible(false);
		first.setVisible(true);
		second.setVisible(true);
	} 
	
	@FXML void TagVisibility(){
		TagType.setVisible(true);
		TagValue.setVisible(true);
		
		FinalizeSearchDate.setVisible(false);
		FinalizeSearchTag.setVisible(false);
		AddToTag.setVisible(true);
		SearchDate.setVisible(false);
		SearchTag.setVisible(true);
		first.setVisible(true);
		second.setVisible(true);
		
	}
	
@FXML 
public void SearchDates() throws ParseException {
	if (!SearchedReturn.isEmpty()) {
		SearchedReturn.removeAll(SearchedReturn); // mot sure if it would work
	}
	LocalDate begin;
	LocalDate  last;
	begin = first.getValue();
	last = second.getValue();
	
	
	
	for (Photo p: album.getPhotos() ) {
		LocalDate d = p.getDate().getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	 if(p.getDate().before(last) && p.getDate().after(begin)) {
		 SearchedReturn.add(p);
	 }
	
	list.addAll(SearchedReturn);
	PhotoTable.setItems(list);
	IntialButton();
	}
	} // Should return an error message 


@FXML
public void initialize() {
	
	 
	ImageFile.setCellValueFactory(new PropertyValueFactory<>("ImageView"));
	PhotoCaption.setCellValueFactory(new PropertyValueFactory<>("caption"));
	
	PhotoTable.getColumns().clear();
	PhotoTable.getColumns().addAll(ImageFile,PhotoCaption);
	PhotoTable.setItems(FXCollections.observableArrayList(list));
	System.out.println( list.size() + " size of photolist"
			);
	
	PhotoTable.setItems(FXCollections.observableArrayList(list));
	
	TagType.setVisible(false);
	TagValue.setVisible(false);

	FinalizeSearchTag.setVisible(false);
	FinalizeSearchDate.setVisible(false);
	AddToTag.setVisible(false);
	first.setVisible(false);
	second.setVisible(false);
}

 @FXML void IntialButton() {
	 TagType.setVisible(false);
		TagValue.setVisible(false);
	
		FinalizeSearchTag.setVisible(false);
		FinalizeSearchDate.setVisible(false);
		AddToTag.setVisible(false);
		SearchDate.setVisible(true);
		SearchTag.setVisible(true);
		first.setVisible(false);
		second.setVisible(false);
		
 }
 
 @FXML
	public void cancel() throws FileNotFoundException, IOException { // need to add log out button
		TagsToSearch.clear();
	IntialButton();
	}
	
	
	
	@FXML public void GoBack() throws FileNotFoundException, IOException {
		Admin.write();
		Photos.UserView();
	}

@FXML  void CreateViaSearch() {
	String g = newAlbname.getText();
	
	Album n = new Album(g);
	
	for (Photo p : SearchedReturn) {
		n.addPhoto(p);
	}
	
	user.addAlbum(n);
}


@FXML 
public void SearchTags() { // be used when finalize TagSearch is used 
	SearchedReturn.clear();
	
	for (int k = 0; k<album.getPhotos().size(); k++) {  Photo p = album.getPhotos().get(k); boolean belongs = true;
	for (int t=0; t<TagsToSearch.size(); t++) {
	Tag currtag = TagsToSearch.get(t);
		for (int i=0; i<p.getTags().size(); i++) {
		if (!p.getTags().get(i).equals(currtag)) { 
			belongs = false;
			break;
		}
	}
		
}
	if (belongs==true && !(SearchedReturn.contains(p))) {
		SearchedReturn.add(p);
	}
}

	
	list.addAll(SearchedReturn);
	PhotoTable.setItems(list);
	IntialButton();
	}

@FXML 
public void CollectTags() { // well be called when Users clicks on Add Tag To Search
	
String tagtype =  TagType.getText();
String tagvalue =  TagValue.getText();

TagsToSearch.add(new Tag(tagtype,tagvalue));

TagType.clear();
TagValue.clear();
if (!TagsToSearch.isEmpty())FinalizeSearchTag.setVisible(true);
else FinalizeSearchTag.setVisible(false);

}
}