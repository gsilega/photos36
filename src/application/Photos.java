package application;
	
import java.io.IOException;
import java.util.List;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import model.Admin;
import model.Album;
import model.User;
import view.AdminViewController;
import view.AlbumViewController;
import view.LoginControl;
import view.RootLayoutController;
import view.UserViewController;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Photos extends Application {
	
	private static Stage primaryStage;
	private static BorderPane rootLayout;
    private static User currentUser;
    private List<User> userList = Admin.getUserList();
    
    public static Stage getPrimaryStage() {
        return primaryStage;
    }
    
    public static BorderPane getRootLayout() {
        return rootLayout;
    }
    
    public static void SetUser(User u) {currentUser = u;}
    
    private Album currentAlbum; 
    
    public void SetAlbum(Album a) { currentAlbum = a;}
    
    
    
    @Override
	public void start(Stage primaryStage) {
    	this.primaryStage = primaryStage;
		try {
			userList = Admin.getUserList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			userList = Admin.getUserList();
		}
        this.primaryStage.setTitle("Photo Album");
        initRootLayout();
        showLoginView();
	}
	
	public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Photos.class.getResource("/view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            // Give the controller access to the main app.
            RootLayoutController controller = loader.getController();
            controller.setMain(this);
            
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public static void UserView() {
        try {
            // Load login
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Photos.class.getResource("/view/Main_Application_Screen.fxml"));
            AnchorPane UserScene = (AnchorPane) loader.load();
            // Set  overview into the center of root layout.
            rootLayout.setCenter(UserScene);
            UserViewController controller = loader.getController();
            controller.setMain(currentUser);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	
	
	public static void showLoginView() {
        try {
            // Load login
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Photos.class.getResource("/view/login.fxml"));
            AnchorPane login = (AnchorPane) loader.load();
            // Set  overview into the center of root layout.
            rootLayout.setCenter(login);
            LoginControl controller = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	public static void main(String[] args) {
	
		launch(args);
	}

	public static void showAdminView() throws ClassNotFoundException {
        try {
            // Load AdminView
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Photos.class.getResource("/view/Admin_Subsystem.fxml"));
            AnchorPane AdminView = (AnchorPane) loader.load();
            // Set  overview into the center of root layout.
            rootLayout.setCenter(AdminView);
            AdminViewController controller = loader.getController();
            controller.setMain();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
/*public void AlbumView() {
	
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("/view/Album_View.fxml"));
          AnchorPane Alb;
		try {
			Alb = (AnchorPane) loader.load();
			 rootLayout.setCenter(Alb);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
        
		AlbumViewController controller = loader.getController();
		System.out.println(this);
		System.out.println(currentUser.getUsername());
		System.out.println(currentAlbum.getAlbumName());
		controller.setMain(this,currentAlbum, currentUser);
    }*/
	
	
}
