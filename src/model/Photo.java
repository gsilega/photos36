package model;

import java.io.Serializable;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Photo implements Serializable{
	// TODO: UPDATE GENERATED SERIALUID AFTER PHOTO IS TOTALLY IMPLEMENTED
	private static final long serialVersionUID = 8464047780639736628L;
	
	/** Caption associated with photo */
	private String caption;
	/** Map of tag string literal to tag object */
	transient private ImageView imageview = new ImageView();
	private ArrayList<Tag> tagList;
	/** Calendar object to hold date photo was taken */
	private Date dateTaken;
	Calendar date;
	/** File object allowing access to the actual photo image file */
	private File pictureFile;

	
	/**
	 * Constructor
	 * @param name of the user uploading the photo
	 * @param uploader's username
	 * @param user provided string to serve as photo caption
	 * @param picture File containing the image associated with this picture
	 * @param initial album that the photo is a member of
	 */
	public Photo(String caption, File pictureImage) {
		
		this.caption = caption;
		this.setPictureFile(pictureImage);
		tagList = new ArrayList<Tag>();
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.MILLISECOND, 0);
		this.dateTaken = new Date(pictureImage.lastModified());
		 imageview = new ImageView();
			date = Calendar.getInstance();
			date.setTime(dateTaken);
			date.set(Calendar.MILLISECOND, 0);
	}
	
	/** Getters and Setters below */

	/**
	 * returns the photo caption
	 * @return caption
	 */
	public String getCaption() {
		return this.caption;
	}
	/**
	 * sets the photo caption to a new message
	 * @param newCaption
	 */
	public void setCaption(String newCaption) {
		this.caption = newCaption;
	}
	/**
	 * returns the Map from tagType strings to Tag objects associated with this Photo
	 * @return tagMap
	 */
	public ArrayList<Tag>getTags() {
		return this.tagList;
	}
	
	public String TagtoString() {
		String fin = "";
	//	System.out.println(tagList.size());
		for (Tag t: tagList) {
			//System.out.println(t);
			fin+=t.getTagType() + " " + t.getTagValue() + "\n"; 
		}
		System.out.println(fin);
		return fin;
	
	}
	/**
	 * sets the Map from tagType strings to Tag objects associated with this Photo
	 * @param tags
	 */
	public void setTags(ArrayList<Tag> tags) {
		tagList = tags;
	}
	/**
	 * adds a tag to the tagType -> Tag object map associated with this Photo
	 * @param tag
	 */
	public void addTag(Tag tag) { 
		for (Tag t: tagList) {
			if(t.equals(tag)) return;
		}
		tagList.add(tag);
	}
	/**
	 * removes a tag from the tagType -> Tag object map associated with this Photo
	 * @param tag
	 */
	public void removeTag(Tag tag) { Tag l = new Tag("",""); boolean found = false;
		for (Tag g : tagList) {
			if(g.equals(tag)) {
				l = g;
				found = true;
			}
		}
		System.out.println("Found : " + found);
		if(found==true)
		tagList.remove(l);
		else return;
	}
	
	public Calendar getDate() {
		return date; 
	}
	
	public String getDateAsString() {
		DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		return dateFormatter.format(this.dateTaken);
	}
	/**
	 * sets the date the photo was taken from a well-formed string with format "MM/dd/yyyy-HH:mm:ss"
	 * @param date of format "MM/dd/yyyy-HH:mm:ss"
	 * @throws ParseException 
	 */
	public static Date getDateFromString(String date) throws ParseException {
	if (isValidFormat(date)) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		Date d = dateFormatter.parse(date);
		return d;
	}
	else return null;
	}
	
	 public static boolean isValidFormat(String value) { // Use for search Date Method 
	        Date date = null;
	        SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy-HH:mm:ss");
	        try {
	        
	            date = dateFormatter.parse(value);
	            if (!value.equals(dateFormatter.format(date))) {
	                date = null;
	            }
	        } catch (ParseException ex) {
	            ex.printStackTrace();
	        }
	        return date != null;
	    }
	/**
	 * returns the Image object associated with this photo
	 * @return
	 */
	public File getPictureFile() {
		return pictureFile;
	}
	/**
	 * sets the Image object associated with this photo to pictureFile
	 * @param pictureFile
	 */
	public void setPictureFile(File pictureFile) {
		this.pictureFile = pictureFile;
	}

	public ImageView getImageView() {
		Image image = new Image(pictureFile.toURI().toString(),50, 50,false, false);
		imageview.setImage(image);
		return imageview;
	}
	
	public ImageView getDisplayView() {
		Image image = new Image(pictureFile.toURI().toString());
		imageview.setImage(image);
		return imageview;
	}
	public void setImageView(ImageView imageview) {
		this.imageview = imageview;
	}

	public void SetDate(Calendar date2) {
		this.date=date2;
		
	}

}
