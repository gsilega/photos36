package model;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
	// TODO: generate new serialVersionUID at the end for the final version of the photo album app
		private static final long serialVersionUID = -2940757050594448598L;
		/** User's username */
		private String username;
		/** List of albums owned by the User */
		private ArrayList<Album> albumList;
		private ArrayList<String> albumNames;
		
		/**
		 * Constructor
		 * @param username
		 * @param givenName
		 */
		public User(String username) {
			this.username = username;
			this.albumList = new ArrayList<Album>();
			this.albumNames = new ArrayList<String>();
		}
		/**
		 * returns the username of this User
		 * @return username
		 */
		public String getUsername() {
			return this.username;
		}
		/**
		 * sets the username of this User to newUsername
		 * @param newUsername
		 */
		public void setUsername(String newUsername) {
			this.username = newUsername;
		}
		
		/**
		 * returns the list of albums owned by this User
		 * @return albumList
		 */
		public ArrayList<Album> getAlbumList() {
			return this.albumList;
		}
		public ArrayList<String> getAlbumNames() {
			return this.albumNames;
		}
		
		public Album getAlbum(String g) {
			return albumList.get(albumNames.indexOf(g));
		}
		/**
		 * adds an album to this list of albums owned by this User
		 * @param album
		 */
		public void addAlbum(Album album) {
			this.albumList.add(album);
			this.albumNames.add(album.getAlbumName());
		}
	
		public String getName(Album b) {
			return albumNames.get(albumList.indexOf(b));
		}
		

		
		public void update(Album b) {
			int x=0;
			for (Album c : this.albumList) {
				if (c.getAlbumName().equals(b.getAlbumName())) {
					x = albumList.indexOf(c);
					break;
				}
			}
			albumList.remove(x);
			albumList.add(x,b);
		}
		/**
		 * deletes an album from the list of albums owned by the User, if it contains the album
		 * @param album
		 */
		public void deleteAlbum(Album album) {
			if (this.albumList.contains(album)) {
				this.albumList.remove(album);
			}
		}
}
