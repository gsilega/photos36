package model;

import java.io.Serializable;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.ArrayList;
public class Admin implements Serializable {
	// TODO: generate a new serialVersion UID on final version of app
		private static final long serialVersionUID = 1896722391212854774L;
		
		private static ArrayList<User> UserList = new ArrayList<User>();
		private static ArrayList<String> names =new ArrayList<String>();
		

	public static ArrayList<User> getUserList(){ return UserList;}

	public static ArrayList<String> getUserListNames(){
		for (User user : UserList) {
			if (!names.contains(user.getUsername())) names.add(user.getUsername());
		}
	return names;
	}
		
	public static void addUser(User u) {
		UserList.add(u);
		names.add(u.getUsername());
	}
	
	public static User returnUser(String g) {
	User u = UserList.get(0);
	for (User a : UserList) {
		if (a.getUsername().equals(g))u = a;
	}
	return u; 
	}
		public void addUser(String g) {
			UserList.add(new User(g));
			names.add(g);
		}
 // Going to change this so it accepts String arg most likely
		public void removeUser(User u) {
			if (this.containsUser(u.getUsername())) {
				int index = FindUser(u.getUsername());
				UserList.remove(index);
			//	names.remove(index);
			}
		}
		
		public static boolean containsUser(String username) {
			for (User user : UserList) {
				if (user.getUsername().equals(username)) return true;
			}
			return false;
		}
		public int FindUser(String username) {
			for (User user : UserList) {
				if (user.getUsername().equals(username)) return UserList.indexOf(user);
			}
		return -1;
		}
		
		public User loginAs(String username) {
			for (User user : UserList) {
				if (user.getUsername().equals(username)) return user;
			}
			return null;
		}
		
		   
		   public static String UserNames() { String fin = "";
			for (User u: UserList) {
				fin+= u.getUsername() + " ";
			}
			return fin;
		   }
		   
		   public static void read() throws IOException, ClassNotFoundException {
			   ObjectInputStream ois = new ObjectInputStream(new FileInputStream("serialized.dat"));
			   UserList = (ArrayList<User>) ois.readObject();
			 for (User u : UserList) {
				for(Album z : u.getAlbumList()) {
					ArrayList<Photo> addList = new ArrayList<Photo>();
					ArrayList<Photo> removeList = new ArrayList<Photo>();
					for (Photo p : z.getPhotos()) {
						Photo temp = new Photo(p.getCaption(), p.getPictureFile());
						temp.SetDate(p.getDate());
						temp.setTags(p.getTags());
						addList.add(temp);
						removeList.add(p);
					}
					z.getPhotos().removeAll(removeList);
					z.getPhotos().addAll(addList);
				}
			 }
			   ois.close();
			   System.out.println(UserNames());
			/*   for (User u : UserList) {
				   System.out.println(" Album list " + u.getAlbumNames());
			   }*/
		   }
		   
		   public static void write() throws FileNotFoundException, IOException {
			   ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("serialized.dat"));
			   oos.writeObject(UserList);
			   oos.close();
			   System.out.println(UserNames());
		   }
		   

		
}
